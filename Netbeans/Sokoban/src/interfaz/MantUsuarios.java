/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaz;

import Excepciones.ExcepcionUsuario;
import superClases.Usuario;
import java.awt.Image;
import java.awt.Toolkit;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import clasesUsuario.Administrador;
import clasesUsuario.Jugador;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author Administrador
 */
public class MantUsuarios extends javax.swing.JFrame {

    private char tipoUsuario;

    private Usuario usuarioSeleccionado;

    /**
     * Creates new form frmMantUsuarios
     *
     * @param pTipoUsuario
     */
    public MantUsuarios(char pTipoUsuario) {
        initComponents();
        openFileFoto.addChoosableFileFilter(new FileNameExtensionFilter("Imágenes", "jpg", "png", "bmp"));
        this.tipoUsuario = pTipoUsuario;
        this.setLocationRelativeTo(null);
        Image image = Toolkit.getDefaultToolkit().getImage(getClass().getResource("/imagenes/usuarios.png"));
        setIconImage(image);
        cargarUsuarios();
        if (pTipoUsuario == 'A') {
            pnlJugador.setVisible(false);
        } else {
            pnlAdministrador.setVisible(false);
        }
    }

    private void cargarUsuarios() {
        DefaultTableModel model = (DefaultTableModel) grdUsuarios.getModel();
        model.setRowCount(0);
        ArrayList<Usuario> usuarios = new ArrayList<>();

        if (tipoUsuario == 'A') {
            usuarios = sokoban.Sokoban.obtenerUsuariosAdministradores();
        } else {
            usuarios = sokoban.Sokoban.obtenerUsuariosJugadores();
        }

        Object rowData[] = new Object[3];
        for (Usuario usuario : usuarios) {
            rowData[0] = usuario.getCodUsuario();
            rowData[1] = usuario.getNombreCompleto();
            SimpleDateFormat sdfTime = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            rowData[2] = sdfTime.format(usuario.getFechaRegistro());
            model.addRow(rowData);
        }

        grdUsuarios.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (grdUsuarios.getSelectedRow() > -1) {
                    //sacamos el valor del codigo de usuario
                    String lCodUsuario = grdUsuarios.getValueAt(grdUsuarios.getSelectedRow(), 0).toString();
                    usuarioSeleccionado = sokoban.Sokoban.buscarUsuario(lCodUsuario);
                    if (usuarioSeleccionado != null) {
                        mostrarInformacionUsuarioSeleccionado();
                    } else {
                        limpiarInformacionUsuario();
                    }

                }
            }
        });

        if (model.getRowCount() > 0) {
            if (usuarioSeleccionado != null) {
                for (int i = 0; i < grdUsuarios.getRowCount(); i++) {
                    if (grdUsuarios.getValueAt(i, 0) == usuarioSeleccionado.getCodUsuario()) {
                        grdUsuarios.changeSelection(i, 0, false, false);
                    }
                }

            } else {
                grdUsuarios.changeSelection(0, 0, false, false);
            }
        }
    }

    private void limpiarInformacionUsuario() {
        this.txtCodUsuario.setText("");
        this.txtPassword.setText("");
        this.txtNombre.setText("");
        this.txtCedula.setText("");
        this.txtEmail.setText("");
        this.txtFechaRegistro.setText("");
        this.lblImagen.setText("FOTO");
        this.lblImagen.setIcon(null);
        this.txtCantNivelesAgregados.setText("");
        this.txtCantBestMovesSuperados.setText("");
        this.cmbNacionalidad.setSelectedItem(null);
    }

    private void mostrarInformacionUsuarioSeleccionado() {
        this.txtCodUsuario.setText(usuarioSeleccionado.getCodUsuario());
        this.txtPassword.setText(usuarioSeleccionado.getPassword());
        this.txtNombre.setText(usuarioSeleccionado.getNombreCompleto());
        this.txtCedula.setText(usuarioSeleccionado.getCedula());
        this.txtEmail.setText(usuarioSeleccionado.geteMail());
        SimpleDateFormat sdfTime = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        this.txtFechaRegistro.setText(sdfTime.format(usuarioSeleccionado.getFechaRegistro()));
        if (usuarioSeleccionado.getFotografia() != null) {
            ImageIcon icon = new ImageIcon(usuarioSeleccionado.getFotografia().getScaledInstance(100, 100, Image.SCALE_DEFAULT));
            this.lblImagen.setText("");
            this.lblImagen.setIcon(icon);
        } else {
            this.lblImagen.setText("FOTO");
            this.lblImagen.setIcon(null);
        }

        if (usuarioSeleccionado instanceof Administrador) {
            this.txtCantNivelesAgregados.setText(String.valueOf(((Administrador) usuarioSeleccionado).getCantNivelesAgregados()));
            this.txtCantBestMovesSuperados.setText(String.valueOf(((Administrador) usuarioSeleccionado).getCantBestMovesSuperados()));
            this.cmbNacionalidad.setSelectedItem(((Administrador) usuarioSeleccionado).getNacionalidad());
        }
        else if(usuarioSeleccionado instanceof Jugador){
            this.chkRompioRecord.setSelected(((Jugador)usuarioSeleccionado).isRompioRecord());
            this.txtCantMayorRepeticiones.setText(String.valueOf(((Jugador)usuarioSeleccionado).getMayorRepeticiones()));
        }

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        openFileFoto = new javax.swing.JFileChooser();
        btnGroup = new javax.swing.ButtonGroup();
        pnlDatosGenerales = new javax.swing.JPanel();
        txtCodUsuario = new javax.swing.JTextField();
        txtPassword = new javax.swing.JPasswordField();
        txtNombre = new javax.swing.JTextField();
        txtCedula = new javax.swing.JFormattedTextField();
        txtEmail = new javax.swing.JTextField();
        txtFechaRegistro = new javax.swing.JTextField();
        btnCambioImagen = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        lblImagen = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        pnlJugador = new javax.swing.JPanel();
        chkRompioRecord = new javax.swing.JCheckBox();
        txtCantMayorRepeticiones = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        pnlAdministrador = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        cmbNacionalidad = new javax.swing.JComboBox<>();
        jLabel9 = new javax.swing.JLabel();
        txtCantNivelesAgregados = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        txtCantBestMovesSuperados = new javax.swing.JTextField();
        scrollGrid = new javax.swing.JScrollPane();
        grdUsuarios = new javax.swing.JTable();
        menu = new javax.swing.JToolBar();
        btnNuevo = new javax.swing.JButton();
        separador1 = new javax.swing.JToolBar.Separator();
        btnEliminar = new javax.swing.JButton();
        separador2 = new javax.swing.JToolBar.Separator();
        btnModificar = new javax.swing.JButton();
        separador3 = new javax.swing.JToolBar.Separator();
        btnSalir = new javax.swing.JButton();

        openFileFoto.setAcceptAllFileFilterUsed(false);
        openFileFoto.setApproveButtonText("Seleccionar");
        openFileFoto.setApproveButtonToolTipText("Seleccionar el archivo seleccionado para imagen del jugador");
        openFileFoto.setDialogTitle("Seleccione la Imagen para el Usuario");

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Mantenimiento de Usuarios");

        try {
            txtCedula.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("#########")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        txtFechaRegistro.setEditable(false);
        txtFechaRegistro.setEnabled(false);

        btnCambioImagen.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/cambiar_imagen.png"))); // NOI18N
        btnCambioImagen.setToolTipText("Cagar una imagen");
        btnCambioImagen.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCambioImagenActionPerformed(evt);
            }
        });

        jLabel2.setText("Nombre:");

        jLabel3.setText("Cédula:");

        jLabel4.setText("Email:");

        jLabel1.setText("Código:");

        jLabel5.setText("Password:");

        lblImagen.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblImagen.setText("FOTO");

        jLabel6.setText("Fecha de Registro:");

        javax.swing.GroupLayout pnlDatosGeneralesLayout = new javax.swing.GroupLayout(pnlDatosGenerales);
        pnlDatosGenerales.setLayout(pnlDatosGeneralesLayout);
        pnlDatosGeneralesLayout.setHorizontalGroup(
            pnlDatosGeneralesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlDatosGeneralesLayout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addGroup(pnlDatosGeneralesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel4)
                    .addComponent(jLabel2)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlDatosGeneralesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlDatosGeneralesLayout.createSequentialGroup()
                        .addGroup(pnlDatosGeneralesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtCodUsuario, javax.swing.GroupLayout.DEFAULT_SIZE, 206, Short.MAX_VALUE)
                            .addComponent(txtNombre))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(pnlDatosGeneralesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.TRAILING)))
                    .addGroup(pnlDatosGeneralesLayout.createSequentialGroup()
                        .addComponent(txtEmail, javax.swing.GroupLayout.PREFERRED_SIZE, 206, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel6)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlDatosGeneralesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(txtCedula, javax.swing.GroupLayout.DEFAULT_SIZE, 194, Short.MAX_VALUE)
                    .addComponent(txtFechaRegistro)
                    .addComponent(txtPassword))
                .addGroup(pnlDatosGeneralesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlDatosGeneralesLayout.createSequentialGroup()
                        .addGap(77, 77, 77)
                        .addComponent(btnCambioImagen)
                        .addGap(44, 44, 44))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlDatosGeneralesLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblImagen, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(27, 27, 27))))
        );
        pnlDatosGeneralesLayout.setVerticalGroup(
            pnlDatosGeneralesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlDatosGeneralesLayout.createSequentialGroup()
                .addGroup(pnlDatosGeneralesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlDatosGeneralesLayout.createSequentialGroup()
                        .addGap(16, 16, 16)
                        .addGroup(pnlDatosGeneralesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtCodUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel1)
                            .addComponent(jLabel5)
                            .addComponent(txtPassword, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlDatosGeneralesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3)
                            .addComponent(txtCedula, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlDatosGeneralesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtEmail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel6)
                            .addComponent(txtFechaRegistro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel4)))
                    .addGroup(pnlDatosGeneralesLayout.createSequentialGroup()
                        .addComponent(lblImagen, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnCambioImagen)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        chkRompioRecord.setText("Rompio Record");
        chkRompioRecord.setEnabled(false);

        txtCantMayorRepeticiones.setEditable(false);

        jLabel7.setText("Mayor cantidad Repeticiones:");

        javax.swing.GroupLayout pnlJugadorLayout = new javax.swing.GroupLayout(pnlJugador);
        pnlJugador.setLayout(pnlJugadorLayout);
        pnlJugadorLayout.setHorizontalGroup(
            pnlJugadorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlJugadorLayout.createSequentialGroup()
                .addGap(91, 91, 91)
                .addComponent(chkRompioRecord)
                .addGap(146, 146, 146)
                .addComponent(jLabel7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txtCantMayorRepeticiones, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pnlJugadorLayout.setVerticalGroup(
            pnlJugadorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlJugadorLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlJugadorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtCantMayorRepeticiones, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7)
                    .addComponent(chkRompioRecord))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabel8.setText("Nacionalidad:");

        cmbNacionalidad.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Costa Rica", "Nicaragua", "Panamá", "España", "Holanda", "Francia", "Otro" }));

        jLabel9.setText("Cant. Niveles Agregados:");

        txtCantNivelesAgregados.setEditable(false);
        txtCantNivelesAgregados.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        jLabel10.setText("Cant. Best Moves Superados:");

        txtCantBestMovesSuperados.setEditable(false);
        txtCantBestMovesSuperados.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        javax.swing.GroupLayout pnlAdministradorLayout = new javax.swing.GroupLayout(pnlAdministrador);
        pnlAdministrador.setLayout(pnlAdministradorLayout);
        pnlAdministradorLayout.setHorizontalGroup(
            pnlAdministradorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlAdministradorLayout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addComponent(jLabel8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cmbNacionalidad, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 61, Short.MAX_VALUE)
                .addComponent(jLabel9)
                .addGap(12, 12, 12)
                .addComponent(txtCantNivelesAgregados, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel10)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtCantBestMovesSuperados, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(21, 21, 21))
        );
        pnlAdministradorLayout.setVerticalGroup(
            pnlAdministradorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlAdministradorLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlAdministradorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlAdministradorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel9)
                        .addComponent(txtCantNivelesAgregados, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(pnlAdministradorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtCantBestMovesSuperados, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel10))
                    .addGroup(pnlAdministradorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel8)
                        .addComponent(cmbNacionalidad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        grdUsuarios.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Código", "Nombre", "Fecha Registro"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        grdUsuarios.setShowHorizontalLines(true);
        grdUsuarios.setShowVerticalLines(true);
        grdUsuarios.getTableHeader().setReorderingAllowed(false);
        scrollGrid.setViewportView(grdUsuarios);
        if (grdUsuarios.getColumnModel().getColumnCount() > 0) {
            grdUsuarios.getColumnModel().getColumn(0).setPreferredWidth(10);
        }

        menu.setFloatable(false);
        menu.setRollover(true);

        btnNuevo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/nuevo_usuario.png"))); // NOI18N
        btnNuevo.setToolTipText("Agregar un nuevo usuario");
        btnNuevo.setFocusable(false);
        btnNuevo.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnNuevo.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoActionPerformed(evt);
            }
        });
        menu.add(btnNuevo);
        menu.add(separador1);

        btnEliminar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/eliminar_usuario.png"))); // NOI18N
        btnEliminar.setToolTipText("Eliminar el usuario seleccionado");
        btnEliminar.setFocusable(false);
        btnEliminar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnEliminar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarActionPerformed(evt);
            }
        });
        menu.add(btnEliminar);
        menu.add(separador2);

        btnModificar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/modificar_usuario.png"))); // NOI18N
        btnModificar.setToolTipText("Guardar los cambios para el usuario seleccionado");
        btnModificar.setFocusable(false);
        btnModificar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnModificar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnModificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModificarActionPerformed(evt);
            }
        });
        menu.add(btnModificar);
        menu.add(separador3);

        btnSalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/cerrar_ventana.png"))); // NOI18N
        btnSalir.setToolTipText("Salir del mantenimiento de Usuarios");
        btnSalir.setFocusable(false);
        btnSalir.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnSalir.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirActionPerformed(evt);
            }
        });
        menu.add(btnSalir);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnlJugador, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(pnlAdministrador, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(scrollGrid)
                    .addComponent(pnlDatosGenerales, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(menu, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(menu, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pnlDatosGenerales, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(154, 154, 154)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(pnlJugador, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(pnlAdministrador, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(scrollGrid, javax.swing.GroupLayout.DEFAULT_SIZE, 400, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnCambioImagenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCambioImagenActionPerformed
        // TODO add your handling code here:
        openFileFoto.setCurrentDirectory(new File(System.getProperty("user.home")));
        openFileFoto.setFileSelectionMode(JFileChooser.FILES_ONLY);
        int result = openFileFoto.showOpenDialog(this);
        if (result == JFileChooser.APPROVE_OPTION) {
            File selectedFile = openFileFoto.getSelectedFile();
            BufferedImage img;
            try {
                img = ImageIO.read(selectedFile);
                ImageIcon icon = new ImageIcon(img.getScaledInstance(100, 100, Image.SCALE_DEFAULT));
                this.lblImagen.setText("");
                this.lblImagen.setIcon(icon);
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(this, "Error cargando la fotografia seleccionada: "
                        + ex.getMessage(), "Error cargando fotografía", JOptionPane.ERROR_MESSAGE);
                Logger.getLogger(MantUsuarios.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_btnCambioImagenActionPerformed

    private void btnEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarActionPerformed
        
        if (usuarioSeleccionado != null) {
            int dialogButton = JOptionPane.YES_NO_OPTION;
            int dialogResult = JOptionPane.showConfirmDialog(this, "¿Esta seguro que desea eliminar definitivamente el usuario "
                    + usuarioSeleccionado.getCodUsuario() + "?", "Verificación", dialogButton);
            if (dialogResult == JOptionPane.YES_OPTION) {
                sokoban.Sokoban.eliminarUsuario(usuarioSeleccionado.getCodUsuario());
                usuarioSeleccionado = null;
                cargarUsuarios();
            }
        } else {
            JOptionPane.showMessageDialog(this, "Debe de seleccionar el usuario que desea eliminar",
                    "Usuario NO seleccionado", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnEliminarActionPerformed

    private void btnModificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModificarActionPerformed
        try {
            // TODO add your handling code here:            
            this.usuarioSeleccionado.setCodUsuario(txtCodUsuario.getText());
            this.usuarioSeleccionado.seteMail(txtEmail.getText());
            this.usuarioSeleccionado.setPassword(new String(txtPassword.getPassword()));
            this.usuarioSeleccionado.setNombreCompleto(txtNombre.getText());
            this.usuarioSeleccionado.setCedula(txtCedula.getText());
            if (lblImagen.getIcon() != null) {
                this.usuarioSeleccionado.setFotografia(((ImageIcon) (lblImagen.getIcon())).getImage());
            } else {
                this.usuarioSeleccionado.setFotografia(null);
            }

            if (usuarioSeleccionado instanceof Administrador) {
                ((Administrador) (this.usuarioSeleccionado)).setNacionalidad(String.valueOf(cmbNacionalidad.getSelectedItem()));
            }
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(this, ex.getMessage(),
                    "Error modificando usuario", JOptionPane.ERROR_MESSAGE);
            Logger.getLogger(MantUsuarios.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            cargarUsuarios();
        }
    }//GEN-LAST:event_btnModificarActionPerformed

    private void btnSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirActionPerformed
        // TODO add your handling code here:
        this.dispose();
    }//GEN-LAST:event_btnSalirActionPerformed

    private void btnNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoActionPerformed
        // TODO add your handling code here:
        try {
            Image foto = null;

            if (lblImagen.getIcon() != null) {
                foto = (((ImageIcon) (lblImagen.getIcon())).getImage());
            }

            if (this.tipoUsuario == 'A') {
                this.usuarioSeleccionado = sokoban.Sokoban.crearUsuarioAdministrador(txtCodUsuario.getText(),
                        new String(txtPassword.getPassword()), txtNombre.getText(),
                        txtCedula.getText(), txtEmail.getText(), foto, String.valueOf(cmbNacionalidad.getSelectedItem()));
            } else if (this.tipoUsuario == 'J') {
                this.usuarioSeleccionado = sokoban.Sokoban.crearUsuarioJugador(txtCodUsuario.getText(),
                        txtNombre.getText(), txtCedula.getText(), txtEmail.getText(),
                        new String(txtPassword.getPassword()), foto);
            }
        } catch (ExcepcionUsuario ex) {
            JOptionPane.showMessageDialog(this, ex.getMessage(),
                    "Error insertando usuario", JOptionPane.ERROR_MESSAGE);
            Logger.getLogger(MantUsuarios.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            cargarUsuarios();
        }
    }//GEN-LAST:event_btnNuevoActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCambioImagen;
    private javax.swing.JButton btnEliminar;
    private javax.swing.ButtonGroup btnGroup;
    private javax.swing.JButton btnModificar;
    private javax.swing.JButton btnNuevo;
    private javax.swing.JButton btnSalir;
    private javax.swing.JCheckBox chkRompioRecord;
    private javax.swing.JComboBox<String> cmbNacionalidad;
    private javax.swing.JTable grdUsuarios;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JLabel lblImagen;
    private javax.swing.JToolBar menu;
    private javax.swing.JFileChooser openFileFoto;
    private javax.swing.JPanel pnlAdministrador;
    private javax.swing.JPanel pnlDatosGenerales;
    private javax.swing.JPanel pnlJugador;
    private javax.swing.JScrollPane scrollGrid;
    private javax.swing.JToolBar.Separator separador1;
    private javax.swing.JToolBar.Separator separador2;
    private javax.swing.JToolBar.Separator separador3;
    private javax.swing.JTextField txtCantBestMovesSuperados;
    private javax.swing.JTextField txtCantMayorRepeticiones;
    private javax.swing.JTextField txtCantNivelesAgregados;
    private javax.swing.JFormattedTextField txtCedula;
    private javax.swing.JTextField txtCodUsuario;
    private javax.swing.JTextField txtEmail;
    private javax.swing.JTextField txtFechaRegistro;
    private javax.swing.JTextField txtNombre;
    private javax.swing.JPasswordField txtPassword;
    // End of variables declaration//GEN-END:variables
}
