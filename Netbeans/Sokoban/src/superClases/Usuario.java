/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package superClases;

import Excepciones.ExcepcionUsuario;
import java.awt.Image;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *Clase Usuario
 * @author Administrador
 */
public abstract class Usuario {

    /**
     * Atributos de la clase
     */
    private String codUsuario;
    private String nombreCompleto;
    private String cedula;
    private String eMail;
    private String password;
    private Date fechaRegistro;
    private Image fotografia;

    private String eMailPattern
            = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    private Pattern pattern;
    private Matcher matcher;

    /**
     * Constructor de la clase Usuario
     *
     * @param codUsuario
     * @param nombreCompleto
     * @param cedula
     * @param eMail
     * @param password
     * @param fotografia
     * @throws Excepciones.ExcepcionUsuario
     */
    public Usuario(String codUsuario, String nombreCompleto, String cedula, String eMail,
            String password, Image fotografia) throws ExcepcionUsuario {
        pattern = Pattern.compile(eMailPattern);
        this.setCodUsuario(codUsuario);
        this.setNombreCompleto(nombreCompleto);
        this.setCedula(cedula);
        this.seteMail(eMail);
        this.setPassword(password);
        this.setFotografia(fotografia);
        this.setFechaRegistro(new Date());        
        
        
    }

    /**
     *
     * @return Codigo de Usuario
     */
    public String getCodUsuario() {
        return codUsuario;
    }

    /**
     *
     * @param codUsuario
     * @throws ExcepcionUsuario Genera una excepción en caso que el codigo del usuario ya exista.
     */
    public void setCodUsuario(String codUsuario) throws ExcepcionUsuario {
        if (codUsuario.equals(""))
            throw new ExcepcionUsuario("Debe de indicar un código de usuario");
        if (sokoban.Sokoban.buscarUsuario(codUsuario) == null) {
            this.codUsuario = codUsuario;
        } else {
            throw new ExcepcionUsuario("El código de usuario " + codUsuario + " no se encuentra disponible!!! Esta siendo utilizado por otro Usuario.");
        }
    }

    /**
     *
     * @return
     */
    public String getNombreCompleto() {
        return nombreCompleto;
    }

    /**
     *
     * @param nombreCompleto
     * @throws Excepciones.ExcepcionUsuario
     */
    public void setNombreCompleto(String nombreCompleto) throws ExcepcionUsuario {
        if(nombreCompleto.equals(""))
            throw  new ExcepcionUsuario("Debe de indicar un Nombre para el usuario");
        
        this.nombreCompleto = nombreCompleto;
    }

    /**
     *
     * @return
     */
    public String getCedula() {
        return cedula;
    }

    /**
     *
     * @param cedula
     */
    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    /**
     *
     * @return
     */
    public String geteMail() {
        return eMail;
    }

    /**
     *
     * @param eMail
     * @throws ExcepcionUsuario Genera una excepcion en caso de que el email indicado no 
     * cuente con el formato establecido
     */
    public void seteMail(String eMail) throws ExcepcionUsuario {
        matcher = pattern.matcher(eMail);
        if ((matcher.matches()) || (eMail.equals(""))) {
            this.eMail = eMail;
        } else {
            throw new ExcepcionUsuario("El formato del Email no es válido");
        }
    }

    /**
     *
     * @return
     */
    public String getPassword() {
        return password;
    }

    /**
     *
     * @param password
     * @throws Excepciones.ExcepcionUsuario
     */
    public void setPassword(String password) throws ExcepcionUsuario {
        if (password.equals(""))
            throw new ExcepcionUsuario("Debe de indicar un password para el usuario.");
        this.password = password;
    }

    /**
     *
     * @return
     */
    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    /**
     *
     * @param fechaRegistro
     */
    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    /**
     *
     * @return
     */
    public Image getFotografia() {
        return fotografia;
    }

    /**
     *
     * @param fotografia
     */
    public void setFotografia(Image fotografia) {
        this.fotografia = fotografia;
    }

    @Override
    public String toString() {
        return "Usuario{" + "codUsuario=" + codUsuario + ", nombreCompleto=" + nombreCompleto + ", cedula=" + cedula + ", eMail=" + eMail + ", password=" + password + ", fechaRegistro=" + fechaRegistro + ", fotografia=" + fotografia + '}';
    }

}
