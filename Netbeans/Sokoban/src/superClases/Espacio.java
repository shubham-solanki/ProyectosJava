/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package superClases;

import clasesEspacio.Caja;
import clasesEspacio.CajaPunto;
import clasesEspacio.Disponible;
import clasesEspacio.Pared;
import clasesEspacio.Personaje;
import clasesEspacio.Punto;
import java.awt.Image;
import java.util.ArrayList;

/**
 * Clase Espacio, tipo que contiene la matriz de un juego
 *
 * @author Administrador
 */
public abstract class Espacio {

    private boolean bloquea;
    private boolean desplaza;
    private Image imagen;
    private int x;
    private int y;
    private boolean revisaBloqueo;

    /**
     *
     * @param x
     * @param bloquea Indica si el espacio tiene el atributo de bloquear a otros
     * objetos como lo son a cajas o al personaje
     * @param y
     * @param revisaBloqueo
     * @param desplaza
     */
    public Espacio(int x, int y, boolean bloquea, boolean desplaza, boolean revisaBloqueo) {
        this.setBloquea(bloquea);
        this.setX(x);
        this.setY(y);
        this.setDesplaza(desplaza);
        this.setRevisaBloqueo(revisaBloqueo);

    }

    /**
     *
     * @return
     */
    public boolean isDesplaza() {
        return desplaza;
    }

    /**
     *
     * @param desplaza
     */
    public void setDesplaza(boolean desplaza) {
        this.desplaza = desplaza;
    }

    /**
     *
     * @return
     */
    public int getX() {
        return x;
    }

    /**
     *
     * @param x
     */
    public void setX(int x) {
        this.x = x;
    }

    /**
     *
     * @return
     */
    public int getY() {
        return y;
    }

    /**
     *
     * @param y
     */
    public void setY(int y) {
        this.y = y;
    }

    /**
     *
     * @return
     */
    public boolean isRevisaBloqueo() {
        return revisaBloqueo;
    }

    /**
     *
     * @param revisaBloqueo
     */
    public void setRevisaBloqueo(boolean revisaBloqueo) {
        this.revisaBloqueo = revisaBloqueo;
    }

    /**
     *
     * @return
     */
    public boolean isBloquea() {
        return bloquea;
    }

    /**
     *
     * @param bloquea
     */
    public void setBloquea(boolean bloquea) {
        this.bloquea = bloquea;
    }

    /**
     *
     * @return
     */
    public Image getImagen() {
        return imagen;
    }

    /**
     *
     * @param imagen
     */
    public void setImagen(Image imagen) {
        this.imagen = imagen;
    }

    /**
     *
     * @param pMatriz
     * @param pEspacioAnterior
     * @param pEspacioOriginal
     * @param pEspaciosRevisados
     * @return
     */
    public boolean isBloqueado(ArrayList<ArrayList<Espacio>> pMatriz, Espacio pEspacioAnterior,
            Espacio pEspacioOriginal, ArrayList<Espacio> pEspaciosRevisados) {
        boolean lBloqueadoArriba = false;
        boolean lBloqueadoDerecha = false;
        boolean lBloqueadoAbajo = false;
        boolean lBloqueadoIzquierda = false;

        if ((this instanceof Disponible) || (this instanceof Pared)
                || (!this.bloquea)) {
            return false;
        }
        /*if((this == pEspacioOriginal) && (pEspacioOriginal != pEspacioAnterior))
            return true;*/

        if ((pEspaciosRevisados.indexOf(this)) == -1) {
            pEspaciosRevisados.add(this);
        } else {
            return true;
        }

        //arriba
        if ((this.getX() - 1) >= 0) {
            lBloqueadoArriba = ((pMatriz.get(this.getX() - 1).get(this.getY()).isBloqueado(pMatriz, this, pEspacioOriginal, pEspaciosRevisados))
                    || (pMatriz.get(this.getX() - 1).get(this.getY()) instanceof Pared));
            /*if((pEspaciosRevisados.indexOf(pMatriz.get(this.getX() - 1).get(this.getY()))) == -1){
              pEspaciosRevisados.add(this);
              lBloqueadoArriba = ((pMatriz.get(this.getX() - 1).get(this.getY()).isBloqueado(pMatriz, this,pEspacioOriginal,pEspaciosRevisados)) 
                        || (pMatriz.get(this.getX() - 1).get(this.getY()) instanceof Pared));
            }
            /*if (!pEspacioAnterior.equals(pMatriz.get(this.getX() - 1).get(this.getY()))) {
                
            } else {
                lBloqueadoArriba = true;
            }*/
            if (((this instanceof Punto) || (this instanceof Personaje)) && (!lBloqueadoArriba)) {
                return lBloqueadoArriba;
            }

        } //estoy en la parte superior de la matriz
        else {
            lBloqueadoArriba = true;
        }

        /*se revisa en la misma fila, 1 columna a la derecha*/
        if ((this.getY() + 1) < pMatriz.get(this.getX()).size()) {
            lBloqueadoDerecha = (pMatriz.get(this.getX()).get(this.getY() + 1).isBloqueado(pMatriz, this, pEspacioOriginal, pEspaciosRevisados))
                    || (pMatriz.get(this.getX()).get(this.getY() + 1) instanceof Pared);
            //con una pared
            /*if((pEspaciosRevisados.indexOf(pMatriz.get(this.getX()).get(this.getY()+1))) == -1){
              pEspaciosRevisados.add(this);
              lBloqueadoArriba = ((pMatriz.get(this.getX()).get(this.getY()+1).isBloqueado(pMatriz, this,pEspacioOriginal,pEspaciosRevisados)) 
                        || (pMatriz.get(this.getX()).get(this.getY()+1) instanceof Pared));
            }
            /*if (!pEspacioAnterior.equals(pMatriz.get(this.getX()).get(this.getY() + 1))) {
                lBloqueadoDerecha = (pMatriz.get(this.getX()).get(this.getY() + 1).isBloqueado(pMatriz, this,pEspacioOriginal))
                        || (pMatriz.get(this.getX()).get(this.getY() + 1) instanceof Pared);
            } else {
                lBloqueadoDerecha = true;
            }*/
            if (((this instanceof Punto) || (this instanceof Personaje)) && (!lBloqueadoDerecha)) {
                return lBloqueadoDerecha;
            }

        } else //estoy en la parte derecha de la matriz
        {
            lBloqueadoDerecha = true;
        }

        if ((this instanceof Caja) && (lBloqueadoArriba && lBloqueadoDerecha)) {
            return true;
        }

        /*se revisa en la misma columna, 1 fila abajo*/
        if ((this.getX() + 1) < pMatriz.size()) {
            lBloqueadoAbajo = (pMatriz.get(this.getX() + 1).get(this.getY()).isBloqueado(pMatriz, this, pEspacioOriginal, pEspaciosRevisados))
                    || (pMatriz.get(this.getX() + 1).get(this.getY()) instanceof Pared);
            /*if (!pEspacioAnterior.equals(pMatriz.get(this.getX() + 1).get(this.getY()))) {
                lBloqueadoAbajo = (pMatriz.get(this.getX() + 1).get(this.getY()).isBloqueado(pMatriz, this,pEspacioOriginal))
                        || (pMatriz.get(this.getX() + 1).get(this.getY()) instanceof Pared);
            } else {
                lBloqueadoAbajo = true;
            }*/
            if (((this instanceof Punto) || (this instanceof Personaje)) && (!lBloqueadoAbajo)) {
                return lBloqueadoAbajo;
            }

        } //estoy en la parte inferior de la matriz
        else {
            lBloqueadoAbajo = true;
        }

        if ((this instanceof Caja) && (lBloqueadoDerecha && lBloqueadoAbajo)) {
            return true;
        }

        /*se revisa en la misma fila, 1 columna a la izquierda*/
        if ((this.getY() - 1) >= 0) {
            lBloqueadoIzquierda = (pMatriz.get(this.getX()).get(this.getY() - 1).isBloqueado(pMatriz, this, pEspacioOriginal, pEspaciosRevisados))
                    || (pMatriz.get(this.getX()).get(this.getY() - 1) instanceof Pared);
            /*if (!pEspacioAnterior.equals(pMatriz.get(this.getX()).get(this.getY() - 1))) {
                lBloqueadoIzquierda = (pMatriz.get(this.getX()).get(this.getY() - 1).isBloqueado(pMatriz, this,pEspacioOriginal))
                        ||(pMatriz.get(this.getX()).get(this.getY() - 1) instanceof Pared);
            } else {
                lBloqueadoIzquierda = true;
            }*/
            if (((this instanceof Punto) || (this instanceof Personaje)) && (!lBloqueadoIzquierda)) {
                return lBloqueadoIzquierda;
            }

        } else //estoy en la parte izquierda de la matriz
        {
            lBloqueadoIzquierda = true;
        }

        if ((this instanceof Caja) && (lBloqueadoAbajo && lBloqueadoIzquierda)) {
            return true;
        }

        if ((this instanceof Caja) && (lBloqueadoIzquierda && lBloqueadoArriba)) {
            return true;
        }

        if (((this instanceof Punto) || (this instanceof Personaje))
                && (lBloqueadoArriba && lBloqueadoDerecha && lBloqueadoAbajo && lBloqueadoIzquierda)) {
            return true;
        }
        return false;

    }
    
    public abstract boolean espacioBloqueado(ArrayList<ArrayList<Espacio>> pMatriz,int pCantRevisones,Espacio pEspacioAnterior);
        

    @Override
    public String toString() {
        return "Espacio{" + "bloquea=" + bloquea + ", imagen=" + imagen + '}';
    }

}
