/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clasesEspacio;

import java.awt.Image;
import java.awt.Toolkit;
import java.util.ArrayList;
import superClases.Espacio;

/**
 * Clase para el espacio de tipo Caja
 *
 * @author Administrador
 */
public class Caja extends Espacio {

    /**
     * Constructor de la clase Caja
     *
     * @param x
     * @param y
     * @throws java.lang.Exception
     */
    public Caja(int x, int y) throws Exception {
        super(x, y, true, true, true);
        Image imagen = Toolkit.getDefaultToolkit().getImage(getClass().getResource("/imagenes/radar.png"));
        this.setImagen(imagen);
    }

    @Override
    public boolean espacioBloqueado(ArrayList<ArrayList<Espacio>> pMatriz, int pCantRevisones
            ,Espacio pEspacioAnterior) {
        boolean lBloqueadoArriba = false;
        boolean lBloqueadoDerecha = false;
        boolean lBloqueadoAbajo = false;
        boolean lBloqueadoIzquierda = false;

        //arriba
        if ((this.getX() - 1) >= 0) {
            Espacio espacioArriba = pMatriz.get(this.getX() - 1).get(this.getY());
            if (espacioArriba instanceof Pared) {
                lBloqueadoArriba = true;
            } else if (((espacioArriba instanceof Caja) || (espacioArriba instanceof CajaPunto)
                    )&&(!espacioArriba.equals(pEspacioAnterior))) {
                if (pCantRevisones < sokoban.Sokoban.getConfiguracion().getCajasPorMovimiento()) {
                    lBloqueadoArriba = espacioArriba.espacioBloqueado(pMatriz, (pCantRevisones + 1),this);
                } 
                else {
                    lBloqueadoArriba = true;                }
            }
        } else {
            lBloqueadoArriba = true;
        }

        //derecha
        if ((this.getY() + 1) < pMatriz.get(this.getX()).size()) {
            Espacio espacioDerecha = pMatriz.get(this.getX()).get(this.getY() + 1);
            if (espacioDerecha instanceof Pared) {
                lBloqueadoDerecha = true;
            } else if (((espacioDerecha instanceof Caja)
                    || (espacioDerecha instanceof CajaPunto)
                    )&&(!espacioDerecha.equals(pEspacioAnterior))) {
                if (pCantRevisones < sokoban.Sokoban.getConfiguracion().getCajasPorMovimiento()) {
                    lBloqueadoDerecha = espacioDerecha.espacioBloqueado(pMatriz, (pCantRevisones + 1),this);
                } else {
                    lBloqueadoDerecha = true;
                }
            }
        } else {
            lBloqueadoDerecha = true;
        }

        //revisa que no tenga nada arriba y a la derecha
        if (lBloqueadoArriba && lBloqueadoDerecha) {
            return true;
        }

        //abajo
        if ((this.getX() + 1) < pMatriz.size()) {
            Espacio espacioAbajo = pMatriz.get(this.getX() + 1).get(this.getY());
            if (espacioAbajo instanceof Pared) {
                lBloqueadoAbajo = true;
            } else if (((espacioAbajo instanceof Caja)
                    || (espacioAbajo instanceof CajaPunto)
                    )&&(!espacioAbajo.equals(pEspacioAnterior))) {
                if (pCantRevisones < sokoban.Sokoban.getConfiguracion().getCajasPorMovimiento()) {
                    lBloqueadoAbajo = espacioAbajo.espacioBloqueado(pMatriz, (pCantRevisones + 1),this);
                } else {
                    lBloqueadoAbajo = true;
                }
            }
        } else {
            lBloqueadoAbajo = true;
        }

        if (lBloqueadoDerecha && lBloqueadoAbajo) {
            return true;
        }

        //izquierda
        if ((this.getY() - 1) >= 0) {
            Espacio espacioIzquierda = pMatriz.get(this.getX()).get(this.getY() - 1);
            if (espacioIzquierda instanceof Pared) {
                lBloqueadoIzquierda = true;
            } else if (((espacioIzquierda instanceof Caja)
                    || (espacioIzquierda instanceof CajaPunto)
                    ) && (!espacioIzquierda.equals(pEspacioAnterior))) {
                if (pCantRevisones < sokoban.Sokoban.getConfiguracion().getCajasPorMovimiento()) {
                    lBloqueadoIzquierda = espacioIzquierda.espacioBloqueado(pMatriz, (pCantRevisones + 1),this);
                } else {
                    lBloqueadoIzquierda = true;
                }
            }
        } else {
            lBloqueadoIzquierda = true;
        }

        if (lBloqueadoAbajo && lBloqueadoIzquierda) {
            return true;
        }

        if (lBloqueadoIzquierda && lBloqueadoArriba) {
            return true;
        }

        return false;

    }

}
