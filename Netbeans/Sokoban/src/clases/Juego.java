/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

import Excepciones.ExcepcionNivel;
import clasesUsuario.Jugador;
import java.util.ArrayList;

/**
 *
 * @author Administrador
 */
public class Juego {

    //Niveles y sus resultado del juego
    private ArrayList<JuegoNivel> nivelesJugador;
    //En caso de pertenecer a un grupo entonces se indica en esta variable
    private Grupo grupo;
    //Nivel que se esta Jugando
    private JuegoNivel nivelActual;
    private String estado;

    /**
     * Constructor de la clase Juego
     *
     * @param grupo Grupo al que pertenece el juego, en caso de ser un juego en
     * grupo
     */
    public Juego(Grupo grupo) {
        this.grupo = grupo;
        this.nivelesJugador = new ArrayList<JuegoNivel>();
        this.nivelActual = null;
        this.estado = "En Proceso";

    }

    /**
     *
     * @return Retorna el listado de los niveles jugados
     */
    public ArrayList<JuegoNivel> getNivelesJugador() {
        return nivelesJugador;
    }

    /**
     *
     * @param nivelesJugador Establece el listado de los niveles jugados
     */
    public void setNivelesJugador(ArrayList<JuegoNivel> nivelesJugador) {
        this.nivelesJugador = nivelesJugador;
    }

    /**
     *
     * @return Retorna el grupo al que pertenece el juego
     */
    public Grupo getGrupo() {
        return grupo;
    }

    /**
     *
     * @param grupo Establece el grupo al que pertenece el Juego
     */
    public void setGrupo(Grupo grupo) {
        this.grupo = grupo;
    }

    /**
     *
     * @return Retorna el estado actual del juego
     */
    public String getEstado() {
        return estado;
    }

    /**
     *
     * @param estado Establece el estado actual del juego
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }

    /**
     *
     * @return Retorna el nivel actual que se esta jugando
     */
    public JuegoNivel getNivelActual() {
        return nivelActual;
    }

    /**
     *
     * @param nivelActual Establece el nivel que se esta jugando
     */
    public void setNivelActual(JuegoNivel nivelActual) {
        this.nivelActual = nivelActual;
    }

    /**
     * Inicia un juego
     *
     * @throws Excepciones.ExcepcionNivel
     * @throws java.lang.Exception
     */
    public void iniciarJuego() throws ExcepcionNivel, Exception {
        JuegoNivel jn;
        if (this.grupo != null) {
            jn = new JuegoNivel(this.grupo.getNivelActual());
        } else if (sokoban.Sokoban.getNiveles().size() > 0) {
            jn = new JuegoNivel(sokoban.Sokoban.getNiveles().get(0));
        } else {
            throw new ExcepcionNivel("No hay niveles definidos para iniciar un nuevo juego!!!.");
        }
        this.setNivelActual(jn);
        this.nivelesJugador.add(jn);
        jn.iniciarJuegoNivel();
    }

    /**
     * Pasa al siguiente Nivel
     *
     * @return
     * @throws java.lang.Exception
     */
    public boolean siguienteNivel() throws Exception {
        JuegoNivel jn = null;

        if (grupo != null) {            
            jn = new JuegoNivel(grupo.getNivelActual());
        } else {

            //buscamos cual nivel es el siguiente
            for (Nivel nivel : sokoban.Sokoban.getNiveles()) {
                if (nivel.getNumeroNivel() > this.nivelActual.getNivel().getNumeroNivel()) {
                    jn = new JuegoNivel(nivel);
                    break;
                }
            }
        }

        if (jn != null) {
            this.nivelActual = jn;
            this.nivelesJugador.add(jn);
            jn.iniciarJuegoNivel();
            return true;
        }

        finalizarJuego("Ganado");
        return false;
    }

    /**
     * Repite un juego, en el caso que se terminara todos los niveles. Un máximo
     * de 3 veces.
     *
     * @param juegoNivel Nivel que se desea repetir
     */
    public void repetirNivel(JuegoNivel juegoNivel) {

    }

    /**
     *
     * @param pNivel
     * @throws Exception
     */
    public void reiniciarNivel(Nivel pNivel) throws Exception {
        this.nivelActual.setEstadoPerdido();
        JuegoNivel jn = new JuegoNivel(pNivel);
        this.nivelesJugador.add(jn);
        this.nivelActual = jn;
        ((Jugador) sokoban.Sokoban.getUsuarioActual()).aumentarRepeticiones();
        jn.iniciarJuegoNivel();
    }

    /**
     * Finaliza el juego actual
     *
     * @param pEstado
     */
    public void finalizarJuego(String pEstado) {
        this.nivelActual = null;
        ((Jugador) sokoban.Sokoban.getUsuarioActual()).setJuegoActual(null);
        this.estado = pEstado;
    }

    /**
     * Reinicia un juego en caso de haber quedado inconcluso
     */
    public void continuarJuego() {
        this.nivelActual.iniciarJuegoNivel();
    }

    /*
    * Elimina un nivel jugado para el actual juego
     */
    private void eliminarJuegoNivel(JuegoNivel pJuegoNivel) {

    }

    /**
     * Reemplaza un nivel jugado por otro, en caso de haberlo repetido y haber
     * tenido mejor resultado
     *
     * @param pJuegoNivelAnterior Nivel jugado anteriormente, el que se desea
     * reemplazar
     * @param pNuevoJuegoNivel Nivel nuevo por el cual se desea reemplazar el
     * anterior
     */
    public void reemplazarJuegoNivel(JuegoNivel pJuegoNivelAnterior, JuegoNivel pNuevoJuegoNivel) {

    }

    @Override
    public String toString() {
        return "Juego{" + "nivelesJugador=" + nivelesJugador + ", grupo=" + grupo + ", nivelActual=" + nivelActual + ", estado=" + estado + '}';
    }

}
