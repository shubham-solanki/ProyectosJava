/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

import clasesUsuario.Administrador;
import clasesUsuario.Jugador;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Generación de las estadisticas de un Jugador
 *
 * @author Administrador
 */
public class Estadistica {

    //contabiliza el tiempo total jugado por un jugador
    private int tiempoTotalJugado;

    //indica el nivel más alto alcanzado por el jugador
    private JuegoNivel maximoNivelGanado;

    //indica el nivel con mayor tiempo jugado
    private Nivel nivelMayorTiempoJugado;

    //indica un promedio de niveles ganados contra los totales jugados
    private double winRate;

    /**
     * Constructor de la clase, se incializan los valores.
     */
    public Estadistica() {
        this.maximoNivelGanado = null;
        this.nivelMayorTiempoJugado = null;
        this.tiempoTotalJugado = 0;
        this.winRate = 0;

    }

    /**
     * Obtiene la cantidad de tiempo total acumulado por un jugador en los
     * diferentes juegos
     *
     * @return Tiempo total acumulado de juego
     */
    public int getTiempoTotalJugado() {
        return tiempoTotalJugado;
    }

    /**
     * Establece el valor para el tiempo total acumulado por un jugador
     *
     * @param tiempoTotalJugado Valor del tiempo total acumulado.
     */
    public void setTiempoTotalJugado(int tiempoTotalJugado) {
        this.tiempoTotalJugado = tiempoTotalJugado;
    }

    /**
     *
     * @return Nivel máximo alcanzado por un jugador
     */
    public JuegoNivel getMaximoNivelGanado() {
        return maximoNivelGanado;
    }

    /**
     * Establece el nivel máximo alcanzado por un jugador
     *
     * @param maximoNivelGanado Nivel máximo alcazado
     */
    public void setMaximoNivelGanado(JuegoNivel maximoNivelGanado) {
        this.maximoNivelGanado = maximoNivelGanado;
    }

    /**
     * Obtiene el nivel con la mayor cantidad de tiempo acumulado jugado.
     *
     * @return El nivel con mayor tiempo jugado acumulado
     */
    public Nivel getNivelMayorTiempoJugado() {
        return nivelMayorTiempoJugado;
    }

    /**
     * Establece el nivel con mayor tiempo acumulado jugado
     *
     * @param nivelMayorTiempoJugado Nivel con mayor tiempo acumulado jugado
     */
    public void setNivelMayorTiempoJugado(Nivel nivelMayorTiempoJugado) {
        this.nivelMayorTiempoJugado = nivelMayorTiempoJugado;
    }

    /**
     * Obtiene el valor para el promedio de partidad ganadas
     *
     * @return Promedio de partidas ganadas contra el total jugado
     */
    public double getWinRate() {
        return winRate;
    }

    /**
     * Establece el valor para el promedio de partidas ganadas contra las
     * jugadas
     *
     * @param winRate Valor del promedio de partidas ganadas contra las jugadas
     */
    public void setWinRate(double winRate) {
        this.winRate = winRate;
    }

    @Override
    public String toString() {
        return "Estadistica{" + "tiempoTotalJugado=" + tiempoTotalJugado + ", maximoNivelJugado=" + maximoNivelGanado + ", nivelMayorTiempoJugado=" + nivelMayorTiempoJugado + ", winRate=" + winRate + '}';
    }

    /**
     *
     * @param pJugador
     */
    public void generarEstadistica(Jugador pJugador) {
        double lCantTotalJuegos = 0;
        double lCantJuegosGanados = 0;
        ArrayList<ArrayList<Object>> nivelTiempoAcumulado = new ArrayList<>();

        for (Juego juego : pJugador.getJuegos()) {
            for (JuegoNivel jn : juego.getNivelesJugador()) {
                boolean lNivelAcumulado = false;
                this.tiempoTotalJugado = this.tiempoTotalJugado + jn.getCantSegundos();
                lCantTotalJuegos++;

                if (jn.getNivel() != null) {
                    for (ArrayList<Object> nivelTiempo : nivelTiempoAcumulado) {
                        if (((Nivel) nivelTiempo.get(0)).equals(jn.getNivel())) {
                            int lCantSegundosAcumulados = Integer.parseInt(nivelTiempo.get(1).toString());
                            nivelTiempo.set(1, lCantSegundosAcumulados + jn.getCantSegundos());
                            lNivelAcumulado = true;
                        }
                    }

                    if (!lNivelAcumulado) {
                        ArrayList<Object> nuevoNivelTiempoAcumulado = new ArrayList<>();
                        nuevoNivelTiempoAcumulado.add(jn.getNivel());
                        nuevoNivelTiempoAcumulado.add(jn.getCantSegundos());
                        nivelTiempoAcumulado.add(nuevoNivelTiempoAcumulado);
                    }
                }

                if (jn.getEstado().contains("Ganado")) {
                    if ((this.maximoNivelGanado == null)
                            || ((jn.getNivel() != null)
                            && (jn.getNivel().getNumeroNivel() > this.maximoNivelGanado.getNivel().getNumeroNivel()))) {
                        this.maximoNivelGanado = jn;
                    }

                    lCantJuegosGanados++;
                }
            }
        }

        Collections.sort(nivelTiempoAcumulado, (ArrayList<Object> o1, ArrayList<Object> o2) -> {
            if (Integer.parseInt(o1.get(1).toString()) < Integer.parseInt(o2.get(1).toString())) {
                return 1;
            } else {
                return -1;
            }
        });

        if (nivelTiempoAcumulado.size() > 0) {
            this.nivelMayorTiempoJugado = (Nivel) nivelTiempoAcumulado.get(0).get(0);
        }

        if (lCantTotalJuegos > 0) {
            this.winRate = (double)Math.round(((lCantJuegosGanados / lCantTotalJuegos) * 100)*100) / 100;
        } else {
            this.winRate = -1;
        }

    }

}
