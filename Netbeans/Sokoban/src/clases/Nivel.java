/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

import Excepciones.ExcepcionNivel;
import clasesEspacio.Caja;
import clasesEspacio.Disponible;
import clasesEspacio.Pared;
import clasesEspacio.Personaje;
import clasesEspacio.Punto;
import clasesUsuario.Administrador;
import java.util.ArrayList;
import superClases.Espacio;

/**
 * Clase para los Niveles
 *
 * @author Administrador
 */
public class Nivel {

    //estado de la matriz.
    private ArrayList<ArrayList<Espacio>> estadoInicial;
    //número unico otorgado por el  juego
    private int numeroNivel;
    //cantidad de movimientos optimos para ganar el juego
    private int bestMoves;
    //usuario creador del nivel
    private Administrador usuarioAdministrador;
    //cantidad de puntos que posee el nivel
    private int cantPuntos;

    /**
     * Constructor de la clase Nivel
     *
     * @param numeroNivel Número de nivel
     * @param bestMoves Cantidad de movimientos optimos para ganar el nivel
     * @param usuarioAdministrador Usuario creador del nivel
     * @throws Excepciones.ExcepcionNivel
     * @throws java.lang.Exception
     */
    public Nivel(int numeroNivel, int bestMoves, Administrador usuarioAdministrador) throws ExcepcionNivel, Exception {
        this.setNumeroNivel(numeroNivel);
        this.setBestMoves(bestMoves);
        this.setUsuarioAdministrador(usuarioAdministrador);
        estadoInicial = new ArrayList<ArrayList<Espacio>>();
        cantPuntos = 0;
        this.crearEstadoInicialMatriz();
    }

    /**
     *
     * @return Retorna la matriz del juego
     */
    public ArrayList<ArrayList<Espacio>> getEstadoInicial() {
        return estadoInicial;
    }

    /**
     *
     * @param estadoInicial Establece la matriz del juego
     * @throws Excepciones.ExcepcionNivel
     */
    public void setEstadoInicial(ArrayList<ArrayList<Espacio>> estadoInicial) throws ExcepcionNivel {
        if (verificarUbicacionPersonaje(estadoInicial)) {

            if (!verificarMatrizBloqueada(estadoInicial,null)) {
                this.cantPuntos = verificarCantCajas(estadoInicial);
                this.estadoInicial = estadoInicial;
            } else {
                throw new ExcepcionNivel("La matriz del nivel " + this.getNumeroNivel() + " se encuentra bloqueada, de manera que no tiene solución posible");
            }

        }
    }

    /**
     *
     * @return Retorna el numero del juego
     */
    public int getNumeroNivel() {
        return numeroNivel;
    }

    /**
     *
     * @param numeroNivel Establece el número de juego
     * @throws Excepciones.ExcepcionNivel
     */
    public void setNumeroNivel(int numeroNivel) throws ExcepcionNivel {
        if (numeroNivel > (sokoban.Sokoban.getNiveles().size() + 1)) {
            throw new ExcepcionNivel("El número de nivel no puede superar la cantidad de niveles actuales ("
                    + String.valueOf(sokoban.Sokoban.getNiveles().size() + 1) + ")");
        }
        if (numeroNivel <= 0) {
            throw new ExcepcionNivel("El número del nivel debe ser mayor a 0");
        }
        this.numeroNivel = numeroNivel;
    }

    private void crearEstadoInicialMatriz() throws Exception {
        //se agregan 3 filas
        for (int f = 0; f < 3; f++) {
            ArrayList<Espacio> fila = new ArrayList<>();
            //se agregan 3 columnas
            for (int c = 0; c < 3; c++) {
                Disponible disponible = new Disponible(f, c);
                fila.add(disponible);
            }
            estadoInicial.add(fila);
        }
        Personaje personaje = new Personaje(0, 1);
        Caja caja = new Caja(1, 1);
        Punto punto = new Punto(2, 1);

        estadoInicial.get(0).set(1, personaje);
        estadoInicial.get(1).set(1, caja);
        estadoInicial.get(2).set(1, punto);
        Pared pared = new Pared(0, 0);
        estadoInicial.get(0).set(0, pared);
        pared = new Pared(2, 0);
        estadoInicial.get(2).set(0, pared);
        pared = new Pared(0, 2);
        estadoInicial.get(0).set(2, pared);
        pared = new Pared(2, 2);
        estadoInicial.get(2).set(2, pared);
        this.cantPuntos = 1;
    }

    /**
     *
     * @return Retorna la cantidad de movimientos optimos para el nivel
     */
    public int getBestMoves() {
        return bestMoves;
    }

    /**
     *
     * @param bestMoves Establece la cantidad de movimientos optimos para el
     * nivel
     * @throws Excepciones.ExcepcionNivel
     */
    public void setBestMoves(int bestMoves) throws ExcepcionNivel {
        if (bestMoves <= 0) {
            throw new ExcepcionNivel("El Best Move del nivel debe de ser mayor a 0");
        }
        this.bestMoves = bestMoves;
    }

    /**
     *
     * @return Retorna el usuario creador del nivel
     */
    public Administrador getUsuarioAdministrador() {
        return usuarioAdministrador;
    }

    /**
     *
     * @param usuarioAdministrador Establece el usuario creador del nivel
     * @throws Excepciones.ExcepcionNivel
     */
    public void setUsuarioAdministrador(Administrador usuarioAdministrador) throws ExcepcionNivel {
        if (usuarioAdministrador == null) {
            throw new ExcepcionNivel("Se debe de indicar un usuario administrador creador para el nivel");
        }
        this.usuarioAdministrador = usuarioAdministrador;
    }

    /**
     *
     * @return Retorna la cantidad de puntos del nivel
     */
    public int getCantPuntos() {
        return cantPuntos;
    }

    /**
     * Cambia el tipo de espacio en una ubicación específica de la matriz
     *
     * @param pMatrizTemporal
     * @param pFila Fila en la que se desea cambiar el tipo de espacio
     * @param pColumna Columna en la que se desea cambiar el tipo de espacio
     * @throws java.lang.Exception
     */
    public static void cambiarTipoEspacio(ArrayList<ArrayList<Espacio>> pMatrizTemporal,
            int pFila, int pColumna) throws Exception {
        Espacio tipoEspacioActual = pMatrizTemporal.get(pFila).get(pColumna);
        Espacio tipoEspacioNuevo = null;

        if (tipoEspacioActual instanceof Disponible) {
            tipoEspacioNuevo = new Pared(pFila, pColumna);
        } else if (tipoEspacioActual instanceof Pared) {
            tipoEspacioNuevo = new Caja(pFila, pColumna);
        } else if (tipoEspacioActual instanceof Caja) {
            tipoEspacioNuevo = new Punto(pFila, pColumna);
        } else if (tipoEspacioActual instanceof Punto) {
            tipoEspacioNuevo = new Personaje(pFila, pColumna);
        } else if (tipoEspacioActual instanceof Personaje) {
            tipoEspacioNuevo = new Disponible(pFila, pColumna);
        }

        pMatrizTemporal.get(pFila).set(pColumna, tipoEspacioNuevo);

    }

    /**
     * Verifica que la cantidad de cajas colocadas en la matriz del nivel sea la
     * misma cantidad de puntos.
     *
     * @param pMatriz
     * @return True en caso que la cantidad de cajas sean la misma que la de
     * puntos
     * @throws Excepciones.ExcepcionNivel
     */
    public int verificarCantCajas(ArrayList<ArrayList<Espacio>> pMatriz) throws ExcepcionNivel {
        int cantCajas = 0;
        int cantPuntos = 0;

        for (ArrayList<Espacio> fila : pMatriz) {
            for (Espacio espacio : fila) {
                if (espacio instanceof Caja) {
                    cantCajas++;
                }
                if (espacio instanceof Punto) {
                    cantPuntos++;
                }
            }
        }

        if (cantPuntos == 0) {
            throw new ExcepcionNivel("El nivel debe de tener definido al menos 1 punto");
        }
        if (cantCajas != cantPuntos) {
            throw new ExcepcionNivel("El nivel debe de contar con la misma cantidad de cajas y puntos");
        } else {
            return cantPuntos;
        }
    }

    /**
     * Verifica que el personaje se encuetre ubicado
     *
     * @param pMatriz
     * @return True en caso que el personaje haya sido ubicado
     * @throws Excepciones.ExcepcionNivel
     */
    public boolean verificarUbicacionPersonaje(ArrayList<ArrayList<Espacio>> pMatriz) throws ExcepcionNivel {
        int lCantPersonaje = 0;
        for (ArrayList<Espacio> fila : pMatriz) {
            for (Espacio columna : fila) {
                if (columna instanceof Personaje) {
                    lCantPersonaje++;
                }
                if (lCantPersonaje > 1) {
                    break;
                }
            }
        }
        if (lCantPersonaje == 1) {
            return true;
        } else {
            throw new ExcepcionNivel("La matriz de juego debe de tener definido estrictamente 1 personaje");
        }
    }

    /**
     *
     * @param pMatriz
     * @return
     */
    public boolean verificarMatrizBloqueada(ArrayList<ArrayList<Espacio>> pMatriz,Espacio pEspacio) {

        for (ArrayList<Espacio> fila : pMatriz) {
            for (Espacio espacio : fila) {
                //if ((espacio instanceof Personaje)) {
                    if ((espacio.isRevisaBloqueo()) && ((pEspacio == null)||(espacio.equals(pEspacio)))) {                        
                        if(espacio.espacioBloqueado(pMatriz, 1,espacio))
                            return true;
                    }
                //}
            }
        }
        return false;
    }

    @Override
    public String toString() {
        return "Nivel{" + "estadoInicial=" + estadoInicial + ", numeroNivel=" + numeroNivel + ", bestMoves=" + bestMoves + ", usuarioAdministrador=" + usuarioAdministrador + ", cantPuntos=" + cantPuntos + '}';
    }

}
