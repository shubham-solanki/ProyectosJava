/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clasesUsuario;

import Excepciones.ExcepcionNivel;
import Excepciones.ExcepcionUsuario;
import clases.Estadistica;
import clases.Grupo;
import clases.Juego;
import clases.JuegoNivel;
import superClases.Usuario;
import java.awt.Image;
import java.util.ArrayList;

/**
 * Clase Jugador Tipo de usuario que puede jugar, no puede configurar el juego
 *
 * @author Administrador
 */
public class Jugador extends Usuario {

    //listado de juegos realizados por el jugador
    private ArrayList<Juego> juegos;
    //juego activo
    private Juego juegoActual;
    //indica si ha logrado romper algún record
    private boolean rompioRecord;
    //mayor cantidad de repeticiones
    private int mayorRepeticiones;
    //Estadisticas del jugador
    private Estadistica estadisticas;

    /**
     * Constructor para la clase Jugador
     *
     * @param codUsuario Código único de usuario
     * @param nombreCompleto Nombre completo del usuario
     * @param cedula Cédula del usuario, con formato #########
     * @param eMail Correo electrónico
     * @param password Contraseña
     * @param fotografia Fotografia del usuario
     * @throws Excepciones.ExcepcionUsuario
     */
    public Jugador(String codUsuario, String nombreCompleto, String cedula, String eMail,
            String password, Image fotografia) throws ExcepcionUsuario {
        super(codUsuario, nombreCompleto, cedula, eMail, password, fotografia);
        this.rompioRecord = false;
        this.mayorRepeticiones = 0;
        this.juegos = new ArrayList<Juego>();
        this.juegoActual = null;
        this.estadisticas = null;
    }

    /**
     *
     * @return
     */
    public boolean isRompioRecord() {
        return rompioRecord;
    }

    /**
     *
     * @param rompioRecord
     */
    public void setRompioRecord(boolean rompioRecord) {
        this.rompioRecord = rompioRecord;
    }

    /**
     *
     * @return
     */
    public int getMayorRepeticiones() {
        return mayorRepeticiones;
    }

    /**
     *
     * @param mayorRepeticiones
     */
    public void setMayorRepeticiones(int mayorRepeticiones) {
        this.mayorRepeticiones = mayorRepeticiones;
    }
    
    /**
     *
     */
    public void aumentarRepeticiones(){
        this.mayorRepeticiones++;
    }

    /**
     *
     * @return
     */
    public ArrayList<Juego> getJuegos() {
        return juegos;
    }

    /**
     *
     * @param juegos
     */
    public void setJuegos(ArrayList<Juego> juegos) {
        this.juegos = juegos;
    }

    /**
     *
     * @return
     */
    public Juego getJuegoActual() {
        return juegoActual;
    }

    /**
     *
     * @param juegoActual
     */
    public void setJuegoActual(Juego juegoActual) {
        this.juegoActual = juegoActual;
    }

    /**
     *
     * @return
     */
    public Estadistica getEstadisticas() {
        return estadisticas;
    }

    /**
     *
     * @param estadisticas
     */
    public void setEstadisticas(Estadistica estadisticas) {
        this.estadisticas = estadisticas;
    }

    /**
     *
     * @param pGrupo
     * @throws Excepciones.ExcepcionNivel
     * @throws ExcepcionNivel
     * @throws Exception
     */
    public void crearNuevoJuego(Grupo pGrupo) throws ExcepcionNivel, Exception {
        if (juegoActual == null) {
            Juego juego = new Juego(pGrupo);
            juegos.add(juego);
            juegoActual = juego;
            juegoActual.iniciarJuego();
        } else {
            juegoActual.continuarJuego();
        }
    }

    /**
     *
     * @return
     */
    public boolean ultimoJuegoGanado() {
        if (this.juegos.size() > 0) {
            return (this.juegos.get(juegos.size() - 1).getEstado().equals("Ganado"));
        }
        return false;
    }

    @Override
    public String toString() {
        return "Jugador{" + "juegos=" + juegos + ", juegoActual=" + juegoActual + ", rompioRecord=" + rompioRecord + ", mayorRepeticiones=" + mayorRepeticiones + ", estadisticas=" + estadisticas + '}';
    }

    /*
    * Retorna el juego con el cual se encuentra la mayor diferencia porcentual
    * entre la cantidad de movimientos hechos y los BestMoves
     */
    private JuegoNivel calcularDiferenciaYourMovesBestMoves() {
        return null;
    }
    
    /**
     *
     */
    public void generarEstadisticas(){
      this.estadisticas = new Estadistica();
      this.estadisticas.generarEstadistica(this);
    }

}
